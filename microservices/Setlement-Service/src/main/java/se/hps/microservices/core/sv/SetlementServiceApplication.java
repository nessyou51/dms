package se.hps.microservices.core.sv;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SetlementServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(SetlementServiceApplication.class, args);
	}

}
