package se.hps.microservices.core.bvc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BusinessChecksServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(BusinessChecksServiceApplication.class, args);
	}

}
