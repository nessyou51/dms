package se.hps.microservices.core.Integrity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IntegrityChecksServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(IntegrityChecksServiceApplication.class, args);
	}

}
