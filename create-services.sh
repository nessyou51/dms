#!/usr/bin/env bash

mkdir microservices
cd microservices


spring init \
--boot-version=3.2.0 \
--type=maven-project \
--build=maven \
--java-version=17 \
--packaging=jar \
--name=Gateway-Service \
--package-name=se.hps.microservices.core.gateway \
--groupId=se.hps.microservices.core.gateway \
--dependencies=actuator,webflux,docker-compose,cloud-gateway  \
--version=1.0.0-SNAPSHOT \
Gateway-Service

spring init \
--boot-version=3.2.0 \
--type=maven-project \
--build=maven \
--java-version=17 \
--packaging=jar \
--name=Discovery-Service \
--package-name=se.hps.microservices.core.eurika \
--groupId=se.hps.microservices.core.eurika \
--dependencies=cloud-eureka-server,docker-compose \
--version=1.0.0-SNAPSHOT \
Discovery-Service

spring init \
--boot-version=3.2.0 \
--type=maven-project \
--build=maven \
--java-version=17 \
--packaging=jar \
--name=Config-Service \
--package-name=se.hps.microservices.core.config \
--groupId=se.hps.microservices.core.config \
--dependencies=cloud-config-server,docker-compose \
--version=1.0.0-SNAPSHOT \
Config-Service




spring init \
--boot-version=3.2.0 \
--type=maven-project \
--build=maven \
--java-version=17 \
--packaging=jar \
--name=Integrity-Checks-Service \
--package-name=se.hps.microservices.core.Integrity \
--groupId=se.hps.microservices.core.Integrity \
--dependencies=actuator,kafka,devtools,lombok,docker-compose \
--version=1.0.0-SNAPSHOT \
Integrity-Checks-Service


spring init \
--boot-version=3.2.0 \
--type=maven-project \
--build=maven \
--java-version=17 \
--packaging=jar \
--name=Business-Checks-Service \
--package-name=se.hps.microservices.core.bvc \
--groupId=se.hps.microservices.core.bvc \
--dependencies=actuator,kafka,devtools,lombok,docker-compose,web \
--version=1.0.0-SNAPSHOT \
Business-Checks-Service


spring init \
--boot-version=3.2.0 \
--type=maven-project \
--build=maven \
--java-version=17 \
--packaging=jar \
--name=Business-Checks-Service \
--package-name=se.hps.microservices.core.bvc \
--groupId=se.hps.microservices.core.bvc \
--dependencies=actuator,kafka,devtools,lombok,docker-compose,web,data-jpa,postgresql \
--version=1.0.0-SNAPSHOT \
Storage-Service



spring init \
--boot-version=3.2.0 \
--type=maven-project \
--build=maven \
--java-version=17 \
--packaging=jar \
--name=Setlement-Service \
--package-name=se.hps.microservices.core.sv \
--groupId=se.hps.microservices.core.sv \
--dependencies=actuator,kafka,devtools,lombok,docker-compose,web,data-jpa,postgresql \
--version=1.0.0-SNAPSHOT \
Setlement-Service








cd ..
